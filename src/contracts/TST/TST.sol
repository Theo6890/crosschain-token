// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/draft-ERC20PermitUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/presets/ERC20PresetFixedSupplyUpgradeable.sol"; // is ERC20Burnable
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

import "./ERC20CappedPresetMinterUpgradeable.sol";

// TODO: mint supply to specified address not msg.sender (CREATE2)
contract TST is
    Initializable,
    ERC20PermitUpgradeable,
    ERC20PresetFixedSupplyUpgradeable,
    ERC20CappedPresetMinterUpgradeable
{
    string private _name;
    string internal _symbol;
    uint256 private _totalSupply = 10**7 * 10**18;

    function initialize(string memory name_, string memory symbol_)
        public
        initializer
    {
        _name = name_;
        _symbol = symbol_;

        ERC20PermitUpgradeable.__ERC20Permit_init(_name);
        ERC20PresetFixedSupplyUpgradeable.initialize(
            _name,
            _symbol,
            _totalSupply / 2,
            0x3785E98f251D35C7D95E8ede1cFa9167445CD558
        );
        ERC20CappedPresetMinterUpgradeable.initialize(_totalSupply);
    }

    function _mint(address account, uint256 amount)
        internal
        override(ERC20CappedPresetMinterUpgradeable, ERC20Upgradeable)
    {
        ERC20CappedPresetMinterUpgradeable._mint(account, amount);
    }

}
