# Crosschain token

ETH <> BSC bridge based on Andre's multichain guide: https://andrecronje.medium.com/multichain-dapp-guide-standards-and-best-practices-8fabe2672c60

## CREATE2Deployer

- Deployer account: `0x605E64c1bCC0dc12079c25e8D1F0c80C1385FD5B` **=> strictly for CREATE2Deployer deployment**
- CREATE2Deployer address: `0x3e616A480b6A9bc19a80D8b1C501DA59647FB8DA` **same on both chain => requirement**
- salt: defined randomly by developer - used [number cipher](https://lingojam.com/AlphabettoNumbers)
