const Create2Deployer = artifacts.require("Create2Deployer");
const TST = artifacts.require("TST");

module.exports = async function (deployer, network, accounts) {
  deployer.then(() => {
    let tx;

    return Create2Deployer.deployed().then(async (c2Deployer) => {
      tx = await c2Deployer.deploy(
        TST.bytecode,
        web3.utils.toWei("15131409012021")
      );

      const res = tx.logs[0].args;
      console.log("TST address: ", res);

      if (network == "kovan") {
        const tst = await TST.at(res.addr);
        tst.initialize("Test Token", "TST");
      }

      if (network == "bsctest") {
        const btst = await TST.at(res.addr);
        btst.initialize("bsc Test Token", "bTST");
      }
    });
  });
};
